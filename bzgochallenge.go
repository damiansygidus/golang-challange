package main

import (
	"fmt"
	"strconv"
)

type Slip struct {
	TransactionIDs []int
}

type Transaction struct {
	ID     int
	Amount float64
	Payout bool
}

type AggregatedSlipData struct {
	// Number of transactions per slip
	TransactionCount int
	// The sum of the transaction amounts of this slip
	// (a payout must be subtracted instead of added!)
	TotalAmount float64
}

func countTotalAmount(transactions []Transaction, transactionIDs []int) float64 {
	var total float64 = 0
	for _, id := range transactionIDs {
		if transactions[id].Payout {
			total = total + transactions[id].Amount
		}
	}

	return total
}

func countTransactions(s Slip) int {
	return len(s.TransactionIDs)
}

func translateID(transactions []Transaction, transactionIDs []int) []int {
	var actualId []int
	for _, oldId := range transactionIDs {
		for id, transaction := range transactions {
			if oldId == transaction.ID {
				actualId = append(actualId, id)
			}
		}
	}

	return actualId
}

func createId(s string) int {
	str, _ := strconv.Atoi(s[5:])
	return str
}

func main() {
	// Assume you have two distributed data sources. Your task is to
	// collect all data from these sources and return an aggregated result.
	slips := map[string]Slip{
		"slip_23": Slip{
			TransactionIDs: []int{123, 456},
		},
		"slip_42": Slip{
			TransactionIDs: []int{789},
		},
	}

	transactions := []Transaction{
		{
			ID:     123,
			Amount: 10.01,
			Payout: false,
		},
		{
			ID:     456,
			Amount: 5.01,
			Payout: true,
		},
		{
			ID:     789,
			Amount: 20.1,
			Payout: false,
		},
	}

	result := map[int]AggregatedSlipData{}

	for key, s := range slips {
		newSlip := AggregatedSlipData{
			TransactionCount: countTransactions(s),
			TotalAmount: countTotalAmount(transactions, translateID(transactions, s.TransactionIDs)),
		}
		result[createId(key)] = newSlip
	}

	// Task: Use the two data sources above and create the following result:
	for id, s := range result {
		fmt.Printf("ID: %d { TransactionCount: %d  TotalAmount: %f}\n", id, s.TransactionCount, s.TotalAmount)
	}
}
